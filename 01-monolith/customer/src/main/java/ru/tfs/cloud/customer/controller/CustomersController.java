package ru.tfs.cloud.customer.controller;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tfs.cloud.customer.domain.Customer;
import ru.tfs.cloud.customer.service.CustomersService;

@RestController
@RequestMapping("/api/customer")
@RequiredArgsConstructor
public class CustomersController {
    private final CustomersService customersService;

    @GetMapping
    public List<Customer> getCustomers() {
        return customersService.findAll();
    }

    @GetMapping("/{customerId}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable long customerId) {
        Optional<Customer> optionalCustomer = customersService.findById(customerId);
        if (optionalCustomer.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(optionalCustomer.get());
    }
}
