package ru.tfs.cloud.customer.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Component;
import ru.tfs.cloud.customer.domain.Customer;
import ru.tfs.cloud.customer.domain.Order;

@Component
public class CustomersRepository {
    private final List<Customer> customers = new ArrayList<>();

    public CustomersRepository() {
        this.customers.add(new Customer(1L, "John", List.of(new Order(1L, "Bread", 2))));
        this.customers.add(new Customer(2L, "Bill", List.of(new Order(2L, "Butter", 1))));
        this.customers.add(new Customer(3L, "Harry", List.of(
            new Order(3L, "Bread", 1), new Order(4L, "Tea", 1))));
    }

    public List<Customer> findAll() {
        return Collections.unmodifiableList(customers);
    }

    public Optional<Customer> findById(long id) {
        return customers.stream()
            .filter(customer -> customer.getId() == id)
            .findFirst();
    }
}
