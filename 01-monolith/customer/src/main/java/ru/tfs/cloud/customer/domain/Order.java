package ru.tfs.cloud.customer.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Order {
    private long id;
    private String item;
    private int count;
}
