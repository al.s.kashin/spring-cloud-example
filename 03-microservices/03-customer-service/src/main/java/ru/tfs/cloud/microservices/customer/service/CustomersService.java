package ru.tfs.cloud.microservices.customer.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tfs.cloud.microservices.customer.client.OrderClient;
import ru.tfs.cloud.microservices.customer.domain.Customer;
import ru.tfs.cloud.microservices.customer.dto.CustomerDto;
import ru.tfs.cloud.microservices.customer.dto.OrderDto;
import ru.tfs.cloud.microservices.customer.repository.CustomersRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomersService {
    private final CustomersRepository customersRepository;
    private final OrderClient orderClient;

    public List<CustomerDto> getCustomers() {
        log.info("get all customers");
        List<CustomerDto> customers = customersRepository.findAll()
            .stream()
            .map(this::mapToDto)
            .collect(Collectors.toList());
        log.info("found {} customers", customers.size());
        return customers;
    }

    public Optional<CustomerDto> getCustomer(long id) {
        log.info("get customer with id {}", id);
        return customersRepository.findById(id).map(this::mapToDto);
    }

    private CustomerDto mapToDto(Customer customer) {
        log.info("get orders by customer id {}", customer.getId());
        List<OrderDto> orders = orderClient.getOrders(customer.getId());
        log.info("received {} orders", orders.size());
        return new CustomerDto(customer, orders);
    }

}
