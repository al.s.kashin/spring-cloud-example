package ru.tfs.cloud.microservices.order.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Order {
    private long id;
    private long customerId;
    private String item;
    private int count;
}
