package ru.tfs.cloud.microservices.order.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import ru.tfs.cloud.microservices.order.domain.Order;

@Component
@Slf4j
public class OrdersRepository {
    private final List<Order> orders = new ArrayList<>();

    public OrdersRepository() {
        this.orders.add(new Order(1L, 1L, "Bread", 2));
        this.orders.add(new Order(2L, 2L, "Butter", 1));
        this.orders.add(new Order(3L, 3L, "Bread", 1));
        this.orders.add(new Order(4L, 3L, "Tea", 1));
    }

    public Flux<Order> findByCustomerId(long customerId) {
        List<Order> orders = this.orders.stream()
            .filter(order -> order.getCustomerId() == customerId)
            .collect(Collectors.toList());
        log.info("found {} orders by customer id {}", orders.size(), customerId);
        return Flux.fromIterable(orders);
    }
}
