package ru.tfs.cloud.services.order.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Order {
    private long id;
    private long customerId;
    private String item;
    private int count;
}
