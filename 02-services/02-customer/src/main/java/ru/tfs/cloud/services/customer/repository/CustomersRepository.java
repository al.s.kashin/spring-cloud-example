package ru.tfs.cloud.services.customer.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Component;
import ru.tfs.cloud.services.customer.domain.Customer;

@Component
public class CustomersRepository {
    private final List<Customer> customers = new ArrayList<>();

    public CustomersRepository() {
        this.customers.add(new Customer(1L, "John"));
        this.customers.add(new Customer(2L, "Bill"));
        this.customers.add(new Customer(3L, "Harry"));
    }

    public List<Customer> findAll() {
        return Collections.unmodifiableList(customers);
    }

    public Optional<Customer> findById(long id) {
        return customers.stream()
            .filter(customer -> customer.getId() == id)
            .findFirst();
    }
}
