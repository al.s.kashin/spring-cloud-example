package ru.tfs.cloud.services.customer.controller;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tfs.cloud.services.customer.dto.CustomerDto;
import ru.tfs.cloud.services.customer.service.CustomersService;

@RestController
@RequestMapping("/api/customer")
@RequiredArgsConstructor
public class CustomersController {

    private final CustomersService customersService;

    @GetMapping
    public List<CustomerDto> getCustomers() {
        return customersService.getCustomers();
    }

    @GetMapping("/{customerId}")
    public ResponseEntity<CustomerDto> getCustomerById(@PathVariable long customerId) {
        Optional<CustomerDto> optionalCustomer = customersService.getCustomer(customerId);
        if (optionalCustomer.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(optionalCustomer.get());
    }
}
