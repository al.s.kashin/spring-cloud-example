package ru.tfs.cloud.services.customer.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.tfs.cloud.services.customer.domain.Customer;

@Data
@AllArgsConstructor
public class CustomerDto {
    private long id;
    private String name;
    private List<OrderDto> orders;

    public CustomerDto(Customer customer, List<OrderDto> orders) {
        this.id = customer.getId();
        this.name = customer.getName();
        this.orders = orders;
    }

    public CustomerDto(Customer customer) {
        this.id = customer.getId();
        this.name = customer.getName();
    }
}
